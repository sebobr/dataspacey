'use strict'

const nano = require('nano');
const _ = require('underscore');
const arangojs = require('arangojs').Database;
const colors = require('colors');

// ------- ------- ------- ------- ------- ------- -------

const env = require('./env.json');
const arango_db = new arangojs({
	url: `http://${env.arango.username}:${env.arango.password}@${env.arango.host}:${env.arango.port}`,
	databaseName: env.arango.arango_database
});

// check if the two names required are here
let COLL_1 = null;
let COLL_2 = null;

if (process.argv.length == 4) {
	COLL_1 = process.argv[2];
	COLL_2 = process.argv[3];
} else {
	usage();
	process.exit(1);
}

function usage() {
	const programName = process.argv[1].substring(process.argv[1].lastIndexOf('/') + 1);
	console.log('Usage: ' + programName + ' coll1 coll2');
	console.log('\tgoal:\t\tcount and compare number of rows in two arango collections');
}


async function countRows (coll) {
	let cursor;
	cursor = await arango_db.query('FOR o IN '+ coll + ' COLLECT WITH COUNT INTO length  RETURN length');
	const results = await cursor.all();
	return results[0];
	}

async function compareSize (coll1, coll2) {
	let size1 = await countRows(coll1);
	let size2 = await countRows(coll2);
	console.log(COLL_1 + ' counts ' + size1 + ' documents/objects/rows');
	console.log(COLL_2 + ' counts ' + size2 + ' documents/objects/rows');
	if (size1<size2) {console.log(COLL_2 + ' has ' + (size2-size1) + ' more elements than ' + COLL_1);}
	else if (size2<size1) {console.log(COLL_1 + ' has ' + (size1-size2) + ' more elements than ' + COLL_2);}
	else {console.log('Both tables/collections have the same number of elements');}
	
	return 'OK';
	
}

compareSize(COLL_1, COLL_2)
.then(() => {
	console.log('process finished well');
	//process.exit(0);
})
.catch(() => {
	console.log('something went wrong :/');
	//process.exit(1);
});


