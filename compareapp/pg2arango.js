'use strict';

/**
 * Extracts data from a PosgreSQL table into an ArangoDB collection
 */

// modules
const arangojs = require('arangojs').Database;
//const AQL = require('arangojs').aqlQuery;
const pgp = require('pg-promise')({
	capSQL: true // capitalize all generated SQL
});
//const async = require('async');
//const fs = require("fs");

// config
const env = require('./env.json');

// CLI args (source / destination)
let SRC_TABLE = null;
let DST_COL = null;
let KEY_PROP = null;
if (process.argv.length > 3) {
	SRC_TABLE = process.argv[2];
	DST_COL = process.argv[3];
	if (process.argv.length > 4) {
		KEY_PROP = process.argv[4];
	}
} else {
	usage();
	process.exit(1);
}

function usage() {
	const programName = process.argv[1].substring(process.argv[1].lastIndexOf('/') + 1);
	console.log('Usage: ' + programName + ' source_table dest_collection [column_to_use_as_key]');
	console.log('\tsource_table:\t\tname of the PostgreSQL table to extract data from');
	console.log('\tdest_collection:\tname of the ArangoDB collection to insert data into');
	console.log('\tcolumn_to_use_as_key:\tname of the PostgreSQL column to use as ArangoDB _key⋅s in new documents');
}

// PostgreSQL with pg-promise
const psql = pgp("postgres://" + env.postgres.username + ":" + env.postgres.password + "@" + env.postgres.host + ":" + env.postgres.port + "/" + env.postgres.databaseName);
console.log('connected to PostgreSQL');

// ArangoDB
const arango = new arangojs({
	url: `http://${env.arango.username}:${env.arango.password}@${env.arango.host}:${env.arango.port}`,
	databaseName: env.arango.arango_database
});
//const destCollection = arango.collection(DST_COL);


//check existence of collection in ArangoDB and create it if doesn't exist

console.log('connected to ArangoDB');

//


// loads data from PostgreSQL by slices of 10
async function loadData() {
	const PAGE_SIZE = 10;
	let offset = 0;
	let go_on = true;
	// get slices
	while (go_on) {
		try {
			console.log('- offset ' + offset + ' limit ' + PAGE_SIZE);
			const data = await psql.any('SELECT * FROM ' + SRC_TABLE + ' OFFSET $1 LIMIT $2', [offset, PAGE_SIZE]);
			//console.log(data);
			if (data.length > 0) {
				console.log('got ' + data.length + ' tuples');
				offset += PAGE_SIZE;
				await insertData(data); // async (remove "await") ?
			}
			if (data.length < PAGE_SIZE) {
				// no more results
				console.log('done !');
				go_on = false;
			}
		} catch(e) {
			console.log('Error in PG query: ' + e);
		}
	}
}



// inserts data into ArangoDB
async function insertData(data) {
	var destCollection = arango.collection(DST_COL);
	try {
		await destCollection.create(); // create the collection if it doesn't exist
	} catch (e) {} 
	try {
		await destCollection.get(); // make sure the collection exists now
//		const result = await destCollection.save({some: 'data'});
 	 // everything went fine
	} catch (e) {
	  console.error('Something went wrong', e.stack);
	}

	//console.log(data[0]);
	let docs = [];
	for (let i=0; i < data.length; i++) {
		let doc = data[i];
		// use column as key ?
		if (KEY_PROP !== null) {
			if (doc[KEY_PROP]) {
				doc._key = '' + doc[KEY_PROP]; // _key is always a string
			} else {
				console.log('cannot use empty value [doc.' + KEY_PROP + '] as _key in new doc');
			}
		}
		//doc = transform(doc); // @TODO later (couch2arango's way)
		docs.push(doc);
	}
	// bulk insert
	try {
		await destCollection.import(docs);
		console.log('+ imported ' + docs.length + ' docs');
	} catch(e) {
		console.log('Error in Arango import: ' + e);
	}
}






loadData()
.then(() => {
	console.log('all good :)');
	//process.exit(0);
})
.catch(() => {
	console.log('something went wrong :/');
	//process.exit(1);
});
