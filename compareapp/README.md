***


- Go inside the main folder /compareapp and build and launch with docker-compose (file docker-compose.yml provide configuration:
> docker-compose up -d

This should create three docker containers connected with each other, one containing a PostgreSQL instance, one containing an ArangoDB instance and one containing a NodeJS app.
(check that nothing is running on port 2500, 5432, and 8529 for exposing our instances)
We will access the methods via the NodeJS instance, but you can also check on ArangoDB database at localhost:8529 address with credentials usr: root and pwd: admin .

- We can now access our methods with the command line. To start, check the node container ID with command: 
> docker ps
We will need it for the commands.


-First we are going to create sample data to use. For the sake of simplicity, I created a file that helps us create psql table with 100 rows and the name of columns we want to define and randomly populate them with 5 char strings:
> docker exec [CONTAINER_ID]  node psql_populate_table.js table1 column1 column2 column3


> docker exec [CONTAINER_ID]  node psql_populate_table.js table2 column1 column3 columnn


So now we have the data we want to compare. We are now going to copy it to ArangoDB to be able to compare what we want.

- The file pg2arango.js simply transfer the content of a given psql table to a new ArangoDB collection (equivalent of a table). We can set the identifier we want as primary key in the command, so here we use the _id key that is already the primary key in our psql tables, and that we will use to compare entries.
> docker exec [CONTAINER_ID] node pg2arango.js table1 collection1 _id

> docker exec [CONTAINER_ID] node pg2arango.js table2 collection2 _id

For example, here I transfer two tables, table1 and table2 from psql to arango collection1 and collection2.

Now we can start to compare the content of the tables.
- First point to compare is counting the number of elements in each table. The file countrows.js take two collections in ArangoDB and simply count and compare the numbers of elements/rows.

> docker exec [CONTAINER_ID]  node countrows.js collection1 collection2

- Second point to compare, we can check if some attributes/column_id are similar or diverging from one row to the other.
That is the role of the file checkattributes.js, that will show the number of occurrence for each attribute in each collection, and then will provide a recap of which attributes are unique to each collection or common to both of them.

> docker exec [CONTAINER_ID]  node checkattributes.js collection1 collection2


- Third point we can compare is the content of two collections. This is the role of the comparelements file that will do two things : first one is checking which keys are unique in each collection and provide these keys, and second is for each key in common between the two collections/tables, it will provide a recap of : 
	-which attribute is existing for the element with the same key in one table and not in the other.
	-which attribute is existing for the element with the same key in the two tables, but with a different value.

> docker exec [CONTAINER_ID]  node compareelements.js collection1 collection2

This program is also creating three text files at the execution, in the folder ./output, and containing the information about differences in the data, so it can later potentially be use to correct or modify on of the dataset if needed.

***
Next step would have been to make a server with a structure permitting to access the method from its interface at port 2500 and not only through the command line, but sadly I am more than late on the timeframe I promessed I was going to observe for the exercise.





